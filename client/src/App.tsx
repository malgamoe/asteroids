import { useState } from "react"
import { MantineProvider, Text, Title } from "@mantine/core"
import { DatePickerInput } from "@mantine/dates"
import { QueryClient, QueryClientProvider, useQuery } from "react-query"
import { AsteroidsResponse } from "@server/asteroids/asteroids.types"

const queryClient = new QueryClient()

function App() {
  const [dates, setDates] = useState<[Date | null, Date | null]>([null, null])

  return (
    <MantineProvider withGlobalStyles withNormalizeCSS>
      <QueryClientProvider client={queryClient}>
        <DatePickerInput
          type="range"
          label="Pick dates range"
          placeholder="Pick dates range"
          value={dates}
          onChange={setDates}
          mx="auto"
          maw={400}
        />
        <Asteroids dates={dates} />
      </QueryClientProvider>
    </MantineProvider>
  )
}

function Asteroids({
  dates: [from, to],
}: {
  dates: [Date | null, Date | null]
}) {
  const dateRequestString =
    from === null || to === null
      ? ""
      : `?start_date=${from.toISOString().split("T")[0]}&end_date=${
          to.toISOString().split("T")[0]
        }`

  const { isLoading, data: asteroids } = useQuery(
    [`asteroids`, dateRequestString],
    (): Promise<AsteroidsResponse> =>
      fetch(
        `${import.meta.env.VITE_API_URL}/asteroids${dateRequestString}`
      ).then((res) => res.json()),
    {
      enabled: !(from === null && to !== null) || !(from !== null && to === null)
    }
  )

  if (isLoading) return "Loading..."

  if (!asteroids?.near_earth_objects) return "No objects"

  return Object.entries(asteroids?.near_earth_objects).map(
    ([date, nearEarthObjects]) => {
      return (
        <div key={date}>
          <Title key={date}>{date}</Title>
          {nearEarthObjects.map((nearEarthObject) => {
            return (
              <div key={"container" + date + nearEarthObject.id}>
                <Text key={date + nearEarthObject.id}>
                  {nearEarthObject.name}
                </Text>
              </div>
            )
          })}
        </div>
      )
    }
  )
}

export default App
