select name, email, sum(p.amount) as amount
from users, 
left join (
  select user_id, order.quantity * product.price as amount
  from order
  join products on products.id = order.product_id
) as p on users.id = p.user_id
group by users.id
having sum(p.amount) > 300