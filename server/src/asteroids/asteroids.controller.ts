import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Query,
} from '@nestjs/common';
import { AsteroidsService } from './asteroids.service';

@Controller('asteroids')
export class AsteroidsController {
  constructor(private readonly asteroidsService: AsteroidsService) {}

  @Get()
  getAsteroids(
    @Query('start_date') startDate: string,
    @Query('end_date') endDate: string,
  ) {
    try {
      return this.asteroidsService.getAsteroids(startDate, endDate);
    } catch (e) {
      throw new HttpException(
        'Could not fetch asteroids',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
