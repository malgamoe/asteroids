import { Injectable } from '@nestjs/common';
import { AsteroidsResponse } from './asteroids.types';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AsteroidsService {
  private asteroidUrl: string;
  constructor(
    private readonly httpService: HttpService,
    private configService: ConfigService,
  ) {
    this.asteroidUrl = this.configService.get<string>('ASTEROIDS_URL') || '';
  }

  async getAsteroids(
    startDate?: string,
    endDate?: string,
  ): Promise<AsteroidsResponse> {
    const datesQueryString =
      startDate === undefined || endDate === undefined
        ? ''
        : `&start_date=${startDate}&end_date=${endDate}`;

    const response = await this.httpService.axiosRef.get(
      this.asteroidUrl + datesQueryString,
    );

    return response.data;
  }
}
