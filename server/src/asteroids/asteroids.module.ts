import { Module } from '@nestjs/common';
import { AsteroidsService } from './asteroids.service';
import { AsteroidsController } from './asteroids.controller';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [HttpModule, ConfigModule],
  controllers: [AsteroidsController],
  providers: [AsteroidsService],
})
export class AsteroidsModule {}
