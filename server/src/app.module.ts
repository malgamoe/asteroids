import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AsteroidsModule } from './asteroids/asteroids.module';

@Module({
  imports: [AsteroidsModule, ConfigModule.forRoot({ isGlobal: true })],
  controllers: [],
  providers: [],
})
export class AppModule {}
