# Asteroids

## Requirements

- node.js
- pnpm

## setup

Copy `client/.env.example` to `client/.env`, and `server/.env.example` to `server/.env`

Run `pnpm i`

## run project

Run `pnpm dev`

Go to `http://localhost:5173/`